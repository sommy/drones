### Drones API Assement

Drone application assesment

## Quickstart

Complete the steps below to run the application.

### 1. Run the application

Spring Boot web applications can be run from the command-line. You need to have the Java Development Kit 8 installed for building the application package and running the application.

Build and run the application with the following command in a terminal window in the root directory:

```sh
./mvnw spring-boot:run
```

This will launch the built-in web server on port 8082.

When the application has started, open the [swagger url](http://127.0.0.1:8082/swagger-ui/index.html) and test the endpoints.
