package com.musalaSoft.drones.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.musalaSoft.drones.dto.DroneRequest;
import com.musalaSoft.drones.dto.DroneResponse;
import com.musalaSoft.drones.dto.MedicationRequest;
import com.musalaSoft.drones.repository.DronesResponse;
import com.musalaSoft.drones.service.DroneService;

@Validated
@RestController
@RequestMapping("/drones")
public class DroneController {

	@Autowired
	private DroneService droneService;
	
	@PostMapping
	public ResponseEntity<DroneResponse> registerDrone(@RequestBody @Validated DroneRequest droneRequest) {
		return new ResponseEntity<>(droneService.registerDrone(droneRequest), HttpStatus.CREATED);
	}
	
	@PutMapping("/{droneId}/medications")
	public ResponseEntity<DroneResponse> loadMedications(@PathVariable long droneId, @RequestBody @Valid List<MedicationRequest> medicationRequests) {
		return new ResponseEntity<>(droneService.loadMedications(droneId, medicationRequests), HttpStatus.OK);
	}
	
	@GetMapping("/{droneId}/medications")
	public ResponseEntity<DroneResponse> fetchLoadedMedications(@PathVariable long droneId) {
		return new ResponseEntity<>(droneService.fetchLoadedMedications(droneId), HttpStatus.OK);
	}
	
	@GetMapping("/available")
	public ResponseEntity<DronesResponse> fetchAvailableDrones() {
		return new ResponseEntity<>(droneService.fetchAvailableDrones(), HttpStatus.OK);
	}
	
}
