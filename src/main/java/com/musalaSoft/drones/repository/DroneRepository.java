package com.musalaSoft.drones.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.musalaSoft.drones.model.Drone;
import com.musalaSoft.drones.model.State;

public interface DroneRepository extends JpaRepository<Drone, Long> {
	public List<Drone> findByState(State state);
}
