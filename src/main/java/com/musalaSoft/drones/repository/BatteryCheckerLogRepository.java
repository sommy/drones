package com.musalaSoft.drones.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.musalaSoft.drones.model.BatteryCheckerLog;

public interface BatteryCheckerLogRepository extends JpaRepository<BatteryCheckerLog, Long> {

}
