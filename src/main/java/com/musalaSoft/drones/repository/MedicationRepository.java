package com.musalaSoft.drones.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.musalaSoft.drones.model.Medication;

public interface MedicationRepository extends JpaRepository<Medication, Long> {

}
