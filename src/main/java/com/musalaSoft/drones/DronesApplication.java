package com.musalaSoft.drones;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import com.musalaSoft.drones.config.SchedularTasks;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Drones API", version = "1.0", description = "Drone application assesment"))
public class DronesApplication {
	
	@Autowired
	private SchedularTasks schedularTasks;
	
	@EventListener(ApplicationReadyEvent.class)
	public void doSomethingAfterStartup() {
		schedularTasks.scheduleBatteryLevelChecker();
	}

	public static void main(String[] args) {
		SpringApplication.run(DronesApplication.class, args);
	}

}
