package com.musalaSoft.drones.advice;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.musalaSoft.drones.dto.ApplicationExceptionResponse;
import com.musalaSoft.drones.exception.RequestException;
import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class ApplicationExceptionHandler {
    
	/**
	   * handle controller methods parameter validation exceptions
	   *
	   * @param exception ex
	   * @return wrapped result
	   */
	  @ExceptionHandler
	  @ResponseStatus(HttpStatus.BAD_REQUEST)
	  public Map<String, String> handle(ConstraintViolationException cve) {
	    Set<ConstraintViolation<?>> violations = cve.getConstraintViolations();
	    
	    Map<String, String> violationMap = new HashMap<>();
		    violations.forEach(violation -> {
		    	String PropertyPath = violation.getPropertyPath().toString();
		    	String arr[] = PropertyPath.split("\\.", 2);
		    	String field = arr[1];
		    	violationMap.put(field, violation.getMessage());
		    });
		    log.error("Bad Request: {}", cve.getLocalizedMessage());
            return violationMap;
	    }
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> applicationExceptionHandler(MethodArgumentNotValidException manve) {
        Map<String, String> errorMap = new HashMap<>();
        manve.getBindingResult().getFieldErrors().forEach(error -> {
            errorMap.put(error.getField(), error.getDefaultMessage());
        });
        log.error("Bad Request: {}", manve.getLocalizedMessage());
        return errorMap;
    }
    
    @ExceptionHandler({RequestException.class})
    public ResponseEntity<ApplicationExceptionResponse> badRequestExceptionHandler(RequestException re) {
        ApplicationExceptionResponse exceptionResponse = new ApplicationExceptionResponse(re.getLocalizedMessage());
        log.error("Bad Request: {}", re.getLocalizedMessage());
        return new ResponseEntity<>(
          exceptionResponse, HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler({HttpMessageNotReadableException.class})
    public ResponseEntity<ApplicationExceptionResponse> enumExceptionHandler(HttpMessageNotReadableException hmnre) {
    	ApplicationExceptionResponse exceptionResponse;
    	if(hmnre.getCause() instanceof InvalidFormatException) {
    		InvalidFormatException ife = (InvalidFormatException) hmnre.getCause();
        	if(ife.getTargetType().isEnum()) {
        		exceptionResponse = new ApplicationExceptionResponse(ife.getOriginalMessage());
                log.error("Bad Request: {}", ife.getLocalizedMessage());
                return new ResponseEntity<>(
                  exceptionResponse, HttpStatus.BAD_REQUEST);
        	}
    	}
    	
    	exceptionResponse = new ApplicationExceptionResponse("A bad request was made, please try again");
        log.error("Bad Request: {}", hmnre.getLocalizedMessage());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler({ Exception.class })
    public ResponseEntity<ApplicationExceptionResponse> handleAll(Exception ex) {
        ApplicationExceptionResponse exceptionResponse = new ApplicationExceptionResponse("Something went wrong, Please try again");
        log.error("Error", ex);
        return new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
	
}
