package com.musalaSoft.drones.config;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.musalaSoft.drones.model.BatteryCheckerLog;
import com.musalaSoft.drones.model.Drone;
import com.musalaSoft.drones.repository.BatteryCheckerLogRepository;
import com.musalaSoft.drones.repository.DroneRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@EnableScheduling
public class SchedularTasks {
	
	@Autowired
	private DroneRepository droneRepository;
	
	@Autowired
	private BatteryCheckerLogRepository batteryCheckerLogRepository;
	
	@Scheduled(cron = "* 1 * * * ?")
	public void scheduleBatteryLevelChecker() {
		List<Drone> drones = droneRepository.findAll();
		
		drones.forEach(drone -> {
			
			BatteryCheckerLog batteryCheckerLog = BatteryCheckerLog.builder()
					.drone(drone)
					.battery(drone.getBattery())
					.createDate(LocalDateTime.now())
					.build();

			batteryCheckerLogRepository.save(batteryCheckerLog);
		});
		log.info("Drone battery save successfully");
	}
}
