package com.musalaSoft.drones.dto;

import java.util.Set;

import javax.persistence.OneToMany;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.musalaSoft.drones.model.Medication;
import com.musalaSoft.drones.model.Model;
import com.musalaSoft.drones.model.State;

import lombok.Data;

@Data
public class DroneRequest {
	@NotBlank(message = "serialNumber is required")
	@Size(max = 100, message = "character size should be less then equal to 100")
	private String serialNumber; //(100 characters max)

	@NotNull(message = "model is required")
	private Model model;

	@NotNull(message = "weight is required")
	@Max(value = 500 , message = "Value should be less then equal to 500")
	private int weight; //limit (500gr max);
	
	@NotNull(message = "battery is required")
	private int battery; //capacity (percentage);
}
