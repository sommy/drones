package com.musalaSoft.drones.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class MedicationRequest {
	@NotBlank(message = "message is required")
	@Pattern(regexp = "^[a-zA-Z0-9\\-_ ]*$", message="name can only contain letters, numbers, ‘-‘, ‘_’")
	private String name; //(allowed only letters, numbers, ‘-‘, ‘_’)
	
	@NotNull(message = "message is required")
	private int weight;
	
	@NotBlank(message = "message is required")
	@Pattern(regexp = "^[A-Z0-9\\_ ]*$", message="name can only contain upper case letters, underscore and numbers")
	private String code; //(allowed only upper case letters, underscore and numbers);
	
	private String image;
}
