package com.musalaSoft.drones.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.musalaSoft.drones.dto.DroneRequest;
import com.musalaSoft.drones.dto.DroneResponse;
import com.musalaSoft.drones.dto.MedicationRequest;
import com.musalaSoft.drones.exception.RequestException;
import com.musalaSoft.drones.model.Drone;
import com.musalaSoft.drones.model.Medication;
import com.musalaSoft.drones.model.State;
import com.musalaSoft.drones.repository.DroneRepository;
import com.musalaSoft.drones.repository.DronesResponse;
import com.musalaSoft.drones.repository.MedicationRepository;

@Service
public class DroneService {
	
	@Autowired
	private DroneRepository droneRepository;
	
	@Autowired
	private MedicationRepository medicationRepository;
	
	public DroneResponse registerDrone(DroneRequest droneRequest) {
		Drone drone = Drone.builder()
				.serialNumber(droneRequest.getSerialNumber())
				.model(droneRequest.getModel())
				.weight(droneRequest.getWeight())
				.battery(droneRequest.getBattery())
				.state(State.IDLE)
                .createDate(LocalDateTime.now())
                .updatedDate(LocalDateTime.now())
				.build();
		return new DroneResponse(droneRepository.save(drone), "Drone has been registered successfully");
	}
	
	public DroneResponse loadMedications(long droneId, List<MedicationRequest> medicationRequests) {
		Optional<Drone> droneData = droneRepository.findById(droneId);
		if(droneData.isPresent()) {
			Drone initDrone = droneData.get();
			
			if(initDrone.getBattery() < 25) throw new RequestException("Drone needs to be charged");
			
			initDrone.setState(State.LOADING);
			Drone drone = droneRepository.save(initDrone);
			
			int medicWeightSum = medicationRequests.stream().mapToInt(r -> r.getWeight()).sum();
			
			//Check if drone weight exceeded
			if(drone.getWeight() < medicWeightSum) throw new RequestException("Drone weight exceeded");
			
			List<Medication> medications = new ArrayList<>();
			
			medicationRequests.forEach(request -> {
				
				Medication medication = Medication.builder()
						.name(request.getName())
						.weight(request.getWeight())
						.code(request.getCode())
						.image(request.getImage())
						.drone(drone)
						.loadedDate(LocalDateTime.now())
						.build();
				
				medications.add(medicationRepository.save(medication));
			});
			
			drone.setState(State.LOADED);
			Drone UpdatedDrone = droneRepository.save(drone);
			
			return new DroneResponse(UpdatedDrone, "Medications successfully loaded");
		}
		
		return new DroneResponse(null, "Drone not found");
	}
	
	public DroneResponse fetchLoadedMedications(long droneId) {
		Optional<Drone> droneData = droneRepository.findById(droneId);
		if(droneData.isPresent()) {
			
			return new DroneResponse(droneData.get(), "Loaded medications retrieved successfully");
		}
		
		return new DroneResponse(null, "Drone not found");
	}
	
	public DronesResponse fetchAvailableDrones() {
		return new DronesResponse(droneRepository.findByState(State.IDLE), "Available drones successfully retriveved");
	}
}
