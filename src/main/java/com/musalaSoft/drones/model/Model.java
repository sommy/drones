package com.musalaSoft.drones.model;

public enum Model {
	LIGHTWEIGHT, MIDDLEWEIGHT, CRUISERWEIGHT, HEAVYWEIGHT;
}
