package com.musalaSoft.drones.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class Medication {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private int weight;
	
	@Column(nullable = false)
	private String code; 
	
	private String image;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="drone_id", nullable=false)
	@JsonIgnoreProperties("medications")
    private Drone drone;
	
	@Column(nullable = false)
    private LocalDateTime loadedDate;
}
